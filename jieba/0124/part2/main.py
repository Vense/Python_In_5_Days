import myLexi

with open('data.txt', 'r', encoding = 'UTF-8') as f:
    ev, senti_segs, senti_vals = myLexi.senti(f.read())
    print("\nsenti_segs({}):\n{}\n".format(len(senti_segs), senti_segs))
    print("senti_vals({}):\n{}\n".format(len(senti_vals), senti_vals))
    print("\nev: {}\n".format(ev))