import jieba

def init(file, file_name):
    with open(file_name, 'r', encoding = 'UTF-8') as f:
        if type(file) == type(dict()):
            for data in f.readlines():
                items = data.strip().split(',')
                file[items[1]] = float(items[2]) - 5
    return file

sentiment_words = init(dict(), 'sentimentWords.csv')

def senti(doc):
    senti_vals = []
    senti_segs = []
    word_segs  = jieba.cut(doc)

    for val in word_segs:
        if val in sentiment_words.keys():
            senti_segs.append(val)
            senti_vals.append(sentiment_words[val])

    return round(sum(senti_vals) / len(senti_vals), 2), senti_segs, senti_vals
    