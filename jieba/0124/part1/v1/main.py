import myLexi

with open('data.txt', 'r', encoding = 'UTF-8') as f:
    result, ps, ns = myLexi.senti(f.read())
    print("\n{}\n\n正向詞彙:\n{}\n負向詞彙:\n{}".format(result, ps, ns))