import myLexi

correct    = 0
wrong      = 0
with open('testSentences.csv', 'r', encoding = 'UTF-8') as f:
    wrong_list = []    
    for data in f.readlines():
        _, content, _, senti, _ = data.strip().split(',')
        k, ps, ns = myLexi.senti(content)
        if k * (float(senti)-5) >= 0:
            correct += 1
        else:
            wrong_list.append((content, str(round(float(senti), 2)), str(k)))
            wrong += 1
    
    print("\nCorrect: {}\n Wrong: {}\n".format(correct, wrong))

with open('result.txt', 'w', encoding = 'UTF-8') as f:
    f.writelines("Correct: {}\nWrong: {}\n\nWrong List:\n{}\n".format(correct, wrong, '-' * 120))
    for content, senti, lext_senti_k in wrong_list:
        f.writelines("{} | {} | {}\n".format(lext_senti_k if len(lext_senti_k) == 2 else " " + lext_senti_k, senti if len(senti) == 4 else senti + '0', content))