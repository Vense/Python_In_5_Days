import jieba

def init(file_type, file_name):
    with open(file_name, 'r', encoding = 'UTF-8') as f:
        if type(file_type) == type(set()):
            for data in f.readlines():
                file_type.add(data.strip())
    
    return file_type

positive_words = init(set(), 'positives.txt')
negative_words = init(set(), 'negatives.txt')

def senti(doc):
    posSegs        = []
    negSegs        = []
    
    for word in jieba.cut(doc):
        if word in positive_words:
            posSegs.append(word)
        if word in negative_words:
            negSegs.append(word)
    res = 0
    if len(posSegs) > len(negSegs):
        res = 1
    elif len(posSegs) < len(negSegs):
        res = -1
    

    return res, posSegs, negSegs